#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <stdint.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

#define GRAPHICS_TEMPLATE_PIXEL_NULL   0
#define GRAPHICS_TEMPLATE_PIXEL_BORDER 1
#define GRAPHICS_TEMPLATE_PIXEL_FILL   2

typedef uint32_t pixel_t;
#define GRAPHICS_RGBX(r, g, b) (((r)<<24) | ((g)<<16) | ((b)<<8))

typedef struct {
    float x;
    float y;
    float z;
} coords_3d_t;

typedef struct {
    float yaw;
    float pitch;
} look_angle_t;

typedef struct {
    int x;
    int y;
} screen_coords_t;

typedef struct {
    int minX;
    int maxX;
    int minY;
    int maxY;
} screen_bounds_t;

typedef int template_pixel_t;

typedef struct {
    pixel_t borderColor;
    pixel_t fillColor;
} quad_color_t;

typedef struct {
    coords_3d_t vertices[4];
    quad_color_t color;
} gquad_t;

typedef struct {
    screen_coords_t vertices[4];
} quad_projected_t;

typedef struct {
    coords_3d_t center;
    quad_color_t color;
} cube_t;

typedef struct {
    pixel_t *frameBuffer;
    template_pixel_t *templatePixels;
    cube_t *cubes;
    gquad_t *quads;
    coords_3d_t observerCoords;
    look_angle_t lookAngle;

    int numCubes;
    int maxCubes;
    pixel_t backgroundColor;
} graphics_ctx_t;

void rotate2D(float angle, float *x, float *y);
screen_coords_t project3D(coords_3d_t observer, coords_3d_t object, look_angle_t lookAngle);
quad_projected_t projectQuad(coords_3d_t observer, look_angle_t lookAngle, gquad_t quad);

int checkCoords(screen_coords_t coords);
void putTemplatePixel(template_pixel_t *pixels, screen_coords_t coords, template_pixel_t pixelType);
template_pixel_t getTemplatePixel(template_pixel_t *pixels, screen_coords_t coords);

void clearTemplate(template_pixel_t *pixels, screen_bounds_t bounds);
void drawLine(template_pixel_t *pixels, screen_coords_t origin, screen_coords_t destination);
void borderFill(template_pixel_t *pixels, screen_bounds_t bounds);

// Actual graphics stuff.
void renderTemplate(pixel_t *pixels, template_pixel_t *templatePixels, screen_bounds_t bounds, pixel_t borderColor, pixel_t fillColor);
void drawQuadOutline(template_pixel_t *templatePixels, quad_projected_t projectedQuad, screen_bounds_t bounds);
void drawQuad(pixel_t *pixels, template_pixel_t *templatePixels, coords_3d_t observer, look_angle_t lookAngle, gquad_t quad);

coords_3d_t quadCenter(gquad_t quad);
float distance3D(coords_3d_t observer, coords_3d_t object);
void sortQuads(gquad_t *quads, int n, coords_3d_t observer);
void cubesToQuads(gquad_t *quads, cube_t *cubes, int n);
void clearFrame(pixel_t *pixels, pixel_t backgroundColor);
void renderCubes(graphics_ctx_t *ctx);

int graphicsInit(graphics_ctx_t *ctx, int maxCubes, pixel_t backgroundColor);
void graphicsFree(graphics_ctx_t *ctx);
#endif
