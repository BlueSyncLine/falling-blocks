#include <math.h>
#include <string.h>
#include <malloc.h>

#include "graphics.h"

// Rotate 2D coordinates.
void rotate2D(float angle, float *x, float *y) {
    float nx, ny;

    nx = *x * cosf(angle) - *y * sinf(angle);
    ny = *x * sinf(angle) + *y * cosf(angle);

    *x = nx;
    *y = ny;
}

// Project a 3D point onto the 2D plane.
screen_coords_t project3D(coords_3d_t observer, coords_3d_t object, look_angle_t lookAngle) {
    coords_3d_t relativeCoords, rotatedCoords;
    screen_coords_t screenCoords;

    // Compute the relative coordinates of the object to the observer.
    relativeCoords.x = object.x - observer.x;
    relativeCoords.y = object.y - observer.y;
    relativeCoords.z = object.z - observer.z;
    rotatedCoords = relativeCoords;

    // Rotate according to the yaw and pitch angles.
    rotate2D(lookAngle.yaw, &rotatedCoords.x, &rotatedCoords.z);
    rotate2D(lookAngle.pitch, &rotatedCoords.y, &rotatedCoords.z);

    if (rotatedCoords.z <= 0.0) {
        // Not in range, return dummy coordinates.
        screenCoords.x = -1;
        screenCoords.y = -1;
    } else {
        // Apply the weak perspective projection.
        screenCoords.x = (rotatedCoords.x / rotatedCoords.z + 1.0f) * SCREEN_WIDTH / 2.0f;
        screenCoords.y = (-rotatedCoords.y / rotatedCoords.z + 1.0f) * SCREEN_HEIGHT / 2.0f;
    }

    return screenCoords;
}

// Project a quad's coordinates.
quad_projected_t projectQuad(coords_3d_t observer, look_angle_t lookAngle, gquad_t quad) {
    int i;
    quad_projected_t projectedQuad;

    for (i = 0; i < 4; i++)
        projectedQuad.vertices[i] = project3D(observer, quad.vertices[i], lookAngle);

    return projectedQuad;
}


// Check whether the given coordinates are in bounds.
int checkCoords(screen_coords_t coords) {
    return coords.x >= 0 && coords.x < SCREEN_WIDTH && coords.y >= 0 && coords.y < SCREEN_HEIGHT;
}

// Put a pixel onto the template.
void putTemplatePixel(template_pixel_t *pixels, screen_coords_t coords, template_pixel_t pixelType) {
    if (checkCoords(coords))
        pixels[coords.x + coords.y * SCREEN_WIDTH] = pixelType;
}

// Get a pixel on the template.
template_pixel_t getTemplatePixel(template_pixel_t *pixels, screen_coords_t coords) {
    if (checkCoords(coords)) {
        return pixels[coords.x + coords.y * SCREEN_WIDTH];
    } else {
        return 0;
    }
}

// Clear the rendering template.
void clearTemplate(template_pixel_t *pixels, screen_bounds_t bounds) {
    screen_coords_t coords;

    // Clear only the bounded area.
    for (coords.y = 0; coords.y <= bounds.maxY - bounds.minY; coords.y++)
        for (coords.x = 0; coords.x <= bounds.maxX - bounds.minX; coords.x++)
            putTemplatePixel(pixels, coords, GRAPHICS_TEMPLATE_PIXEL_NULL);
}

// Draw a line.
void drawLine(template_pixel_t *pixels, screen_coords_t origin, screen_coords_t destination) {
    int i, steps;
    float x, y;
    float dx, dy, length, sx, sy;
    screen_coords_t coords;

    // Find the length of the line.
    dx = destination.x - origin.x;
    dy = destination.y - origin.y;
    length = sqrtf(dx * dx + dy * dy);
    steps = ceilf(length);

    // Find the step sizes.
    sx = dx / length;
    sy = dy / length;

    // Start at the origin.
    x = origin.x;
    y = origin.y;

    for (i = 0; i < steps; i++) {
        // Set the pixel coordinates.
        coords.x = x;
        coords.y = y;

        // Draw the pixel.
        putTemplatePixel(pixels, coords, GRAPHICS_TEMPLATE_PIXEL_BORDER);
        x += sx;
        y += sy;
    }
}

// Fill the parts of the template surrounded by the border pixels.
void borderFill(template_pixel_t *pixels, screen_bounds_t bounds) {
    int x, y, firstBorder, lastBorder;
    screen_coords_t coords;

    // Scan vertically.
    for (y = 1; y < bounds.maxY - bounds.minY; y++) {
        coords.y = y;
        firstBorder = -1;
        lastBorder = -1;

        for (x = 0; x <= bounds.maxX - bounds.minX; x++) {
            coords.x = x;

            // Border pixel encountered, count it.
            if (getTemplatePixel(pixels, coords) == GRAPHICS_TEMPLATE_PIXEL_BORDER) {
                if (firstBorder == -1)
                    firstBorder = x;
                else
                    lastBorder = x;
            }
        }

        // Two borders, we can fill that line.
        if (firstBorder >= 0 && lastBorder >= 0) {
            // Draw a line of "fill" pixels.
            for (x = firstBorder + 1; x < lastBorder; x++) {
                coords.x = x;
                putTemplatePixel(pixels, coords, GRAPHICS_TEMPLATE_PIXEL_FILL);
            }
        }
    }
}


// Render the template.
void renderTemplate(pixel_t *pixels, template_pixel_t *templatePixels, screen_bounds_t bounds, pixel_t borderColor, pixel_t fillColor) {
    int x, y, sx, sy, i, j;

    for (y = 0; y <= bounds.maxY - bounds.minY; y++) {
        for (x = 0; x <= bounds.maxX - bounds.minX; x++) {
            sx = bounds.minX + x;
            sy = bounds.minY + y;
            i = x + y * SCREEN_WIDTH;
            j = sx + sy * SCREEN_WIDTH;
            if (templatePixels[i] != GRAPHICS_TEMPLATE_PIXEL_NULL && sx >= 0 && sx <= SCREEN_WIDTH && sy >= 0 && sy <= SCREEN_HEIGHT)
                pixels[j] = templatePixels[i] == GRAPHICS_TEMPLATE_PIXEL_FILL ? fillColor : borderColor;
        }
    }
}

// Draw a quad outline.
void drawQuadOutline(template_pixel_t *templatePixels, quad_projected_t projectedQuad, screen_bounds_t bounds) {
    int i;
    screen_coords_t origin, destination;

    origin = projectedQuad.vertices[0];
    origin.x -= bounds.minX;
    origin.y -= bounds.minY;
    for (i = 1; i < 5; i++) {
        destination = projectedQuad.vertices[i % 4];
        destination.x -= bounds.minX;
        destination.y -= bounds.minY;

        drawLine(templatePixels, origin, destination);
        origin = destination;
    }
}

// Draw a quad.
void drawQuad(pixel_t *pixels, template_pixel_t *templatePixels, coords_3d_t observer, look_angle_t lookAngle, gquad_t quad) {
    int i, x, y, width, height;
    screen_bounds_t bounds;
    quad_projected_t projectedQuad;

    // Project the quad.
    projectedQuad = projectQuad(observer, lookAngle, quad);

    // Check if the quad is visible.
    if (!(checkCoords(projectedQuad.vertices[0]) || checkCoords(projectedQuad.vertices[1]) || checkCoords(projectedQuad.vertices[2]) || checkCoords(projectedQuad.vertices[3])))
        return;

    // Initialize the screen bounds.
    bounds.minX = projectedQuad.vertices[0].x;
    bounds.minY = projectedQuad.vertices[0].y;
    bounds.maxX = projectedQuad.vertices[0].x;
    bounds.maxY = projectedQuad.vertices[0].y;

    // Check the other vertices and find the bounds.
    for (i = 1; i < 4; i++) {
        x = projectedQuad.vertices[i].x;
        y = projectedQuad.vertices[i].y;

        if (x > bounds.maxX)
            bounds.maxX = x;
        else if (x < bounds.minX)
            bounds.minX = x;

        if (y > bounds.maxY)
            bounds.maxY = y;
        else if (y < bounds.minY)
            bounds.minY = y;
    }

    // Compute the width and height of the bounds.
    width = bounds.maxX - bounds.minX + 1;
    height = bounds.maxY - bounds.minY + 1;

    // Check the width and height.
    if (width >= SCREEN_WIDTH || height >= SCREEN_HEIGHT)
        return;

    // Clear the template.
    clearTemplate(templatePixels, bounds);

    // Draw the lines bounding the quad.
    drawQuadOutline(templatePixels, projectedQuad, bounds);

    // Fill.
    borderFill(templatePixels, bounds);

    // Draw the lines again, over the filled areas.
    drawQuadOutline(templatePixels, projectedQuad, bounds);

    renderTemplate(pixels, templatePixels, bounds, quad.color.borderColor, quad.color.fillColor);
}

// Find the center of a quad.
coords_3d_t quadCenter(gquad_t quad) {
    coords_3d_t coords;

    coords.x = (quad.vertices[0].x + quad.vertices[1].x + quad.vertices[2].x + quad.vertices[3].x) / 4.0f;
    coords.y = (quad.vertices[0].y + quad.vertices[1].y + quad.vertices[2].y + quad.vertices[3].y) / 4.0f;
    coords.z = (quad.vertices[0].z + quad.vertices[1].z + quad.vertices[2].z + quad.vertices[3].z) / 4.0f;
    return coords;
}

// Compute distance between two 3D points.
float distance3D(coords_3d_t observer, coords_3d_t object) {
    float dx, dy, dz;
    dx = object.x - observer.x;
    dy = object.y - observer.y;
    dz = object.z - observer.z;

    return sqrtf(dx*dx + dy*dy + dz*dz);
}

// Sort an array of quads in order of decreasing distance to the observer.
void sortQuads(gquad_t *quads, int n, coords_3d_t observer) {
    int i, j;
    float a, b;
    gquad_t temp;

    // Bubble sort
    for (i = 1; i < n; i++) {
        for (j = 0; j < n - i; j++) {
            // Compute the distances.
            a = distance3D(observer, quadCenter(quads[j]));
            b = distance3D(observer, quadCenter(quads[j + 1]));

            // Swap
            if (a < b) {
                temp = quads[j];
                quads[j] = quads[j + 1];
                quads[j + 1] = temp;
            }
        }
    }
}


// Convert cubes to quads corresponding to their faces.
void cubesToQuads(gquad_t *quads, cube_t *cubes, int n) {
    int i, j;
    cube_t cube;
    gquad_t quad;
    coords_3d_t center;

    j = 0;

    // Iterate through all cubes.
    for (i = 0; i < n; i++) {
        cube = cubes[i];
        center = cube.center;

        // Face 0
        quad.color = cube.color;
        quad.vertices[0].x = center.x;
        quad.vertices[0].y = center.y;
        quad.vertices[0].z = center.z;
        quad.vertices[1].x = center.x;
        quad.vertices[1].y = center.y + 1.0f;
        quad.vertices[1].z = center.z;
        quad.vertices[2].x = center.x + 1.0f;
        quad.vertices[2].y = center.y + 1.0f;
        quad.vertices[2].z = center.z;
        quad.vertices[3].x = center.x + 1.0f;
        quad.vertices[3].y = center.y;
        quad.vertices[3].z = center.z;
        quads[j++] = quad;

        // Face 1
        quad.color = cube.color;
        quad.vertices[0].x = center.x;
        quad.vertices[0].y = center.y;
        quad.vertices[0].z = center.z;
        quad.vertices[1].x = center.x;
        quad.vertices[1].y = center.y;
        quad.vertices[1].z = center.z + 1.0f;
        quad.vertices[2].x = center.x;
        quad.vertices[2].y = center.y + 1.0f;
        quad.vertices[2].z = center.z + 1.0f;
        quad.vertices[3].x = center.x;
        quad.vertices[3].y = center.y + 1.0f;
        quad.vertices[3].z = center.z;
        quads[j++] = quad;

        // Face 2
        quad.color = cube.color;
        quad.vertices[0].x = center.x;
        quad.vertices[0].y = center.y;
        quad.vertices[0].z = center.z;
        quad.vertices[1].x = center.x;
        quad.vertices[1].y = center.y;
        quad.vertices[1].z = center.z + 1.0f;
        quad.vertices[2].x = center.x + 1.0f;
        quad.vertices[2].y = center.y;
        quad.vertices[2].z = center.z + 1.0f;
        quad.vertices[3].x = center.x + 1.0f;
        quad.vertices[3].y = center.y;
        quad.vertices[3].z = center.z;
        quads[j++] = quad;

        // Face 3
        quad.color = cube.color;
        quad.vertices[0].x = center.x;
        quad.vertices[0].y = center.y + 1.0f;
        quad.vertices[0].z = center.z;
        quad.vertices[1].x = center.x;
        quad.vertices[1].y = center.y + 1.0f;
        quad.vertices[1].z = center.z + 1.0f;
        quad.vertices[2].x = center.x + 1.0f;
        quad.vertices[2].y = center.y + 1.0f;
        quad.vertices[2].z = center.z + 1.0f;
        quad.vertices[3].x = center.x + 1.0f;
        quad.vertices[3].y = center.y + 1.0f;
        quad.vertices[3].z = center.z;
        quads[j++] = quad;

        // Face 4
        quad.color = cube.color;
        quad.vertices[0].x = center.x + 1.0f;
        quad.vertices[0].y = center.y;
        quad.vertices[0].z = center.z;
        quad.vertices[1].x = center.x + 1.0f;
        quad.vertices[1].y = center.y;
        quad.vertices[1].z = center.z + 1.0f;
        quad.vertices[2].x = center.x + 1.0f;
        quad.vertices[2].y = center.y + 1.0f;
        quad.vertices[2].z = center.z + 1.0f;
        quad.vertices[3].x = center.x + 1.0f;
        quad.vertices[3].y = center.y + 1.0f;
        quad.vertices[3].z = center.z;
        quads[j++] = quad;

        // Face 5
        quad.color = cube.color;
        quad.vertices[0].x = center.x;
        quad.vertices[0].y = center.y;
        quad.vertices[0].z = center.z + 1.0f;
        quad.vertices[1].x = center.x;
        quad.vertices[1].y = center.y + 1.0f;
        quad.vertices[1].z = center.z + 1.0f;
        quad.vertices[2].x = center.x + 1.0f;
        quad.vertices[2].y = center.y + 1.0f;
        quad.vertices[2].z = center.z + 1.0f;
        quad.vertices[3].x = center.x + 1.0f;
        quad.vertices[3].y = center.y;
        quad.vertices[3].z = center.z + 1.0f;
        quads[j++] = quad;
    }
}

// Fill the frame buffer with the background color.
void clearFrame(pixel_t *pixels, pixel_t backgroundColor) {
    int i;

    for (i = 0; i < SCREEN_WIDTH * SCREEN_HEIGHT; i++)
        pixels[i] = backgroundColor;
}

// Clear the frame and draw cubes.
void renderCubes(graphics_ctx_t *ctx) {
    int i;

    // Make quads.
    cubesToQuads(ctx->quads, ctx->cubes, ctx->numCubes);

    // Sort quads.
    sortQuads(ctx->quads, ctx->numCubes * 6, ctx->observerCoords);

    // Clear the frame.
    clearFrame(ctx->frameBuffer, ctx->backgroundColor);

    // Draw the quads.
    for (i = 0; i < ctx->numCubes * 6; i++)
        drawQuad(ctx->frameBuffer, ctx->templatePixels, ctx->observerCoords, ctx->lookAngle, ctx->quads[i]);
}


// Initializes a graphics context.
int graphicsInit(graphics_ctx_t *ctx, int maxCubes, pixel_t backgroundColor) {
    // Allocate the buffers and arrays.
    ctx->frameBuffer = calloc(SCREEN_WIDTH * SCREEN_HEIGHT, sizeof(pixel_t));
    ctx->templatePixels = calloc(SCREEN_WIDTH * SCREEN_HEIGHT, sizeof(template_pixel_t));
    ctx->cubes = calloc(maxCubes, sizeof(cube_t));
    ctx->quads = calloc(maxCubes * 6, sizeof(gquad_t));

    // Allocation error.
    if (ctx->frameBuffer == NULL || ctx->templatePixels == NULL || ctx->cubes == NULL || ctx->quads == NULL) {
        graphicsFree(ctx);
        return 0;
    }

    ctx->numCubes = 0;
    ctx->maxCubes = maxCubes;

    // Initialize the observer coordinates and look angle.
    memset(&ctx->observerCoords, 0, sizeof(coords_3d_t));
    memset(&ctx->lookAngle, 0, sizeof(look_angle_t));

    // Set the background color.
    ctx->backgroundColor = backgroundColor;

    return 1;
}

// Frees up the memory used by a graphics context.
void graphicsFree(graphics_ctx_t *ctx) {
    free(ctx->frameBuffer);
    free(ctx->templatePixels);
    free(ctx->cubes);
    free(ctx->quads);
}
