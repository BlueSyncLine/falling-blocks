#include <stdlib.h>
#include <stdio.h>

#include <math.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include "graphics.h"

#define MAX_FALLING 16
#define MAX_CUBES (MAX_FALLING + 1)
#define FRAME_DELAY 20
#define RANDOM_PROBABILITY 16

#define BGM_PATH "res/ofdm-downlink.mp3"

typedef struct {
    coords_3d_t center;
    int active;
    int captured;
} block_t;

graphics_ctx_t ctx;
SDL_Renderer *renderer;
SDL_Window *window;
SDL_Texture *texture;
Mix_Music *music;

coords_3d_t playerCoords;
block_t fallingBlocks[MAX_FALLING];

int randRange(int min, int max) {
    return min + abs((rand() % (max - min)));
}

void addCube() {
    int i;

    for (i = 0; i < MAX_FALLING; i++) {
        if (!fallingBlocks[i].active) {
            fallingBlocks[i].center.x = randRange(-10, 10);
            fallingBlocks[i].center.z = randRange(10, 20);
            fallingBlocks[i].center.y = 5.0f;
            fallingBlocks[i].active = 1;
            fallingBlocks[i].captured = 0;
            break;
        }
    }
}

void frame() {
    int i;

    // The player's cube.
    ctx.numCubes = 1;
    ctx.cubes[0].center = playerCoords;
    ctx.cubes[0].color.borderColor = GRAPHICS_RGBX(0, 255, 255);
    ctx.cubes[0].color.fillColor = GRAPHICS_RGBX(0, 0, 255);

    // Falling blocks.
    for (i = 0; i < MAX_FALLING; i++) {
        // Inactive slot, skip.
        if (!fallingBlocks[i].active)
            continue;

        // Gravity.
        fallingBlocks[i].center.y -= 0.1f;

        // Is the block below the ground?
        if (fallingBlocks[i].center.y <= -10.0f) {
            fallingBlocks[i].active = 0;
        } else {
            // Captured?
            if (distance3D(playerCoords, fallingBlocks[i].center) < 1.0f)
                fallingBlocks[i].captured = 1;

            // Another cube.
            ctx.cubes[ctx.numCubes].center = fallingBlocks[i].center;
            ctx.cubes[ctx.numCubes].color.borderColor = fallingBlocks[i].captured ? GRAPHICS_RGBX(0, 255, 0) : GRAPHICS_RGBX(32, 32, 32);
            ctx.cubes[ctx.numCubes].color.fillColor = fallingBlocks[i].captured ? GRAPHICS_RGBX(96, 64, 192) : (fallingBlocks[i].center.y >= playerCoords.y ? GRAPHICS_RGBX(64, 96, 64) : GRAPHICS_RGBX(128, 64, 64));
            ctx.numCubes++;
        }
    }

    // Try adding a cube randomly.
    if (rand() % RANDOM_PROBABILITY == 0)
        addCube();

    // Render the frame.
    renderCubes(&ctx);
    SDL_UpdateTexture(texture, NULL, ctx.frameBuffer, SCREEN_WIDTH * sizeof(pixel_t));
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
}


int main(int argc, char *argv[]) {
    int i, finished;
    SDL_Event event;

    if (!graphicsInit(&ctx, MAX_CUBES, GRAPHICS_RGBX(96, 96, 96))) {
        fprintf(stderr, "Couldn't initialize graphics!");
        return EXIT_FAILURE;
    }

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
        graphicsFree(&ctx);
        return EXIT_FAILURE;
    }

    // Try opening the audio mixer.
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0) {
        fprintf(stderr, "Couldn't initialize the mixer: %s\n", Mix_GetError());
        graphicsFree(&ctx);
        SDL_Quit();
        return EXIT_FAILURE;
    }

    if (!(music = Mix_LoadMUS(BGM_PATH))) {
        fprintf(stderr, "Couldn't load the background music file.\n");
        graphicsFree(&ctx);
        Mix_CloseAudio();
        SDL_Quit();
        return EXIT_FAILURE;
    }

    // Try starting the background music.
    if (Mix_PlayMusic(music, -1) < 0) {
        fprintf(stderr, "Couldn't start playing the background music.\n");
        graphicsFree(&ctx);
        Mix_CloseAudio();
        Mix_FreeMusic(music);
        SDL_Quit();
    }

    if (SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, 0, &window, &renderer) < 0) {
        fprintf(stderr, "SDL_CreateWindowAndRenderer failed: %s\n", SDL_GetError());
        graphicsFree(&ctx);
        Mix_CloseAudio();
        Mix_FreeMusic(music);
        SDL_Quit();
        return EXIT_FAILURE;
    }

    if (!(texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBX8888, SDL_TEXTUREACCESS_STREAMING, SCREEN_WIDTH, SCREEN_HEIGHT))) {
        fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
        graphicsFree(&ctx);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        Mix_CloseAudio();
        Mix_FreeMusic(music);
        SDL_Quit();
        return EXIT_FAILURE;
    }

    SDL_SetWindowTitle(window, "pointless surreal game");

    // Reset the player coordinates.
    memset(&playerCoords, 0, sizeof(coords_3d_t));
    playerCoords.y = -5.0f;
    playerCoords.z = 15.0f;

    // Initialize the falling block entities.
    for (i = 0; i < MAX_FALLING; i++)
        fallingBlocks[i].active = 0;

    /*ctx.numCubes = 1;
    ctx.cubes[0].color.borderColor = GRAPHICS_RGBX(0, 0, 128);
    ctx.cubes[0].color.fillColor = GRAPHICS_RGBX(255, 128, 64);
    ctx.cubes[0].center.x = -1.0f;
    ctx.cubes[0].center.y = -5.0f;
    ctx.cubes[0].center.z = 8.0f;*/

    finished = 0;

    while (!finished) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    finished = 1;
                    break;
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                        case SDLK_UP:
                            playerCoords.z += 1.0f;
                            break;
                        case SDLK_DOWN:
                            playerCoords.z -= 1.0f;
                            break;
                        case SDLK_LEFT:
                            playerCoords.x -= 1.0f;
                            break;
                        case SDLK_RIGHT:
                            playerCoords.x += 1.0f;
                            break;
                        case SDLK_LSHIFT:
                            playerCoords.y -= 1.0f;
                            break;
                        case SDLK_LCTRL:
                            playerCoords.y += 1.0f;
                            break;
                        case SDLK_w:
                            ctx.lookAngle.pitch = fmod(ctx.lookAngle.pitch - 0.01f, M_PI * 2.0f);
                            break;
                        case SDLK_s:
                            ctx.lookAngle.pitch = fmod(ctx.lookAngle.pitch + 0.01f, M_PI * 2.0f);
                            break;
                        case SDLK_a:
                            ctx.lookAngle.yaw = fmod(ctx.lookAngle.yaw + 0.01f, M_PI * 2.0f);
                            break;
                        case SDLK_d:
                            ctx.lookAngle.yaw = fmod(ctx.lookAngle.yaw - 0.01f, M_PI * 2.0f);
                            break;
                        case SDLK_q:
                            addCube();
                            break;
                    }
            }
        }

        frame();
        SDL_Delay(FRAME_DELAY);
    }

    // Exit.
    graphicsFree(&ctx);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    Mix_CloseAudio();
    Mix_FreeMusic(music);
    SDL_Quit();
    return EXIT_SUCCESS;
}
