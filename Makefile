all: clean blocks

clean:
	rm -f *.o blocks

blocks: blocks.o
	gcc -o blocks -lm `sdl2-config --libs` -lSDL2_mixer blocks.o graphics.o

blocks.o: graphics.o
	gcc -g -c -o blocks.o -Wall -Wextra `sdl2-config --cflags` main.c

graphics.o:
	gcc -g -c -o graphics.o -Wall -Wextra graphics.c
